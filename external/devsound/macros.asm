; ================================================================
; DevSound macros
; ================================================================

Instrument:		macro
	db	\1
	dw	\2,\3,\4,\5
	endm

Drum:			macro
	db	SetInstrument,\1,fix,\2
	endm

Pointer: MACRO
	db \1	; song speed groove/1
	db \2	; song speed groove/2
	dw \3	; the actual pointer
ENDM
