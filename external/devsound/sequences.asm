SECTION	"DevSound Instruments",ROMX

; =================================================================
; Waves
; =================================================================

WaveTable:
	dw	DefaultWave

; =================================================================
; Instruments
; =================================================================

vib_Dummy:	db	0,0,$80,1
pulse_Dummy:	db	0,$ff

InstrumentTable:
const_value set 0
	dw	ins_Ins0
	const _ins_Ins0

	dw	ins_sfx_PullUpMonitor
	const _ins_sfx_PullUpMonitor

	dw	ins_sfx_SwitchScreens
	const _ins_sfx_SwitchScreens

vol_Ins0:
		rept 4
		db $58	; volume
		endr
		db $FF
arp_Ins0:	db $13,$0,$0,$ff
pulse_Ins0:	db 2,$ff
ins_Ins0:	Instrument 0, vol_Ins0, arp_Ins0, pulse_Ins0, vib_Dummy

s7 equ $2d

vol_sfx_PullUpMonitor:	db	$18,$ff
nsq_sfx_PullUpMonitor:	db	s7+29,0,s7+28,$80,2
ins_sfx_PullUpMonitor:	Instrument 0, vol_sfx_PullUpMonitor, nsq_sfx_PullUpMonitor, DummyTable, DummyTable

vol_sfx_SwitchScreens:	db	$15,$ff
nsq_sfx_SwitchScreens:	db	s7+22,$80,0
ins_sfx_SwitchScreens:	Instrument 0, vol_sfx_SwitchScreens, nsq_sfx_SwitchScreens, DummyTable, DummyTable
