SECTION	"DevSound Song Pointers",ROMX
SongSpeedAndPointerTable:
const_value set 0
	const Song_Sixam
	Pointer	9,10,	PT_Sixam

	const SFX_PullUpMonitor
	Pointer	8,8,	PT_PullUpMonitor

	const SFX_SwitchScreens
	Pointer	8,8,	PT_SwitchScreens

SECTION	"DevSound Songs",ROMX
INCLUDE "external/devsound/songs/sixam.asm"
INCLUDE "external/devsound/songs/sfx.asm"
