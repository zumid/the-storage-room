; ================================================================
; DevSound RAM
; ================================================================

SECTION	"DevSound RAM",WRAM0

GlobalVolume::		db
GlobalSpeed1::		db
GlobalSpeed2::		db
GlobalTimer::		db
TickCount::			db
SoundEnabled::		db

CH1Enabled::		db
CH2Enabled::		db
CH3Enabled::		db
CH4Enabled::		db

CH1Ptr::			dw
CH1VolPtr::			dw
CH1PulsePtr::		dw
CH1ArpPtr::			dw
CH1VibPtr::			dw
CH1RetPtr::			dw
CH1VolPos::			db
CH1PulsePos::		db
CH1ArpPos::			db
CH1VibPos::			db
CH1VibDelay::		db
CH1Tick::			db
CH1Reset::			db
CH1Note::			db
CH1Transpose::		db
CH1FreqOffset::		db
CH1Pan::			db
CH1Sweep::			db
CH1NoteCount::		db
CH1InsMode::		db
CH1Ins1::			db
CH1Ins2::			db

CH2Ptr::			dw
CH2VolPtr::			dw
CH2PulsePtr::		dw
CH2ArpPtr::			dw
CH2VibPtr::			dw
CH2RetPtr::			dw
CH2VolPos::			db
CH2PulsePos::		db
CH2ArpPos::			db
CH2VibPos::			db
CH2VibDelay::		db
CH2Tick::			db
CH2Reset::			db
CH2Note::			db
CH2Transpose::		db
CH2FreqOffset::		db
CH2Pan::			db
CH2NoteCount::		db
CH2InsMode::		db
CH2Ins1::			db
CH2Ins2::			db

CH3Ptr::			dw
CH3VolPtr::			dw
CH3WavePtr::		dw
CH3ArpPtr::			dw
CH3VibPtr::			dw
CH3RetPtr::			dw
CH3VolPos::			db
CH3WavePos::		db
CH3ArpPos::			db
CH3VibPos::			db
CH3VibDelay::		db
CH3Tick::			db
CH3Reset::			db
CH3Note::			db
CH3Transpose::		db
CH3FreqOffset::		db
CH3Vol::			db
CH3Wave::			db
CH3Pan::			db
CH3NoteCount::		db
CH3InsMode::		db
CH3Ins1::			db
CH3Ins2::			db

CH4Ptr::			dw
CH4VolPtr::			dw
CH4NoisePtr::		dw
CH4RetPtr::			dw
CH4VolPos::			db
CH4NoisePos::		db
CH4Mode::			db
CH4Tick::			db
CH4Reset::			db
CH4Transpose::		db
CH4Pan::			db
CH4NoteCount::		db
CH4InsMode::		db
CH4Ins1::			db
CH4Ins2::			db
DSVarsEnd:

WaveBuffer::		ds	16
WavePos::			db
WaveBufUpdateFlag::	db
PWMEnabled::		db
PWMVol::			db
PWMSpeed::			db
PWMTimer::			db
PWMDir::			db

ArpBuffer::			ds	8
