# RGBDS variables
# for if you want to get another version

# remove implicit rules
MAKEFLAGS += -rR

.PHONY: clean clean-dist rom

RGBDSDIR	:= /usr/bin
ASM	:= $(RGBDSDIR)/rgbasm
GFX	:= $(RGBDSDIR)/rgbgfx
LNK	:= $(RGBDSDIR)/rgblink
FIX	:= $(RGBDSDIR)/rgbfix

PYTHON := /usr/bin/python3

ROMNAME  := fnafgb
GAMENAME := "FIVE NIGHTS"
RGBFIX_OPTS := -c -k "ZD" -l 0x33 -m 0x01 -p 0x00
RGBASM_OPTS := -h -DDEBUG

GFXDIR = gfx
GFXSRC = $(shell find $(GFXDIR) -name \*.png)
GFXOUT = $(patsubst $(GFXDIR)/%.png, $(GFXDIR)/%.2bpp, $(GFXSRC))

CAMDIR = cam
CAMSRC = $(shell find $(CAMDIR) -name \*.png)
CAMOUT = $(patsubst $(CAMDIR)/%.png, $(CAMDIR)/%.pb16, $(CAMSRC))

MAPDIR = map
MAPSRC = $(shell find $(MAPDIR) -name \*.map)
MAPOUT = $(patsubst $(MAPDIR)/%.map, $(MAPDIR)/%.map.pb16, $(MAPSRC))

# preserve compressed map files and gfx/2bpp files
.PRECIOUS: $(GFXOUT) %.map.pb16

GITHASH = $(shell git rev-parse --short=8 HEAD | tr '[a-z]' '[A-Z]')

all: rom

rom: $(ROMNAME).gb

%.gb: %.o
	$(LNK) -m $(patsubst %.gb, %.map, $@) -n $(patsubst %.gb, %.sym, $@) -o $@ -l $(patsubst %.gb, %.link, $@) $<
	$(FIX) $(RGBFIX_OPTS) -t $(GAMENAME) -v $@

data/camera_pics.asm: $(CAMDIR) $(CAMOUT)
	$(PYTHON) _scripts/generate_camera_pics_asm.py $< $@

%.o: main.asm $(GFXOUT) $(MAPOUT) $(CAMOUT) engine/ home/ data/camera_pics.asm
	$(ASM) -DGITHASH="\"$(GITHASH)\"" $(RGBASM_OPTS) -o $@ $<

%.2bpp: %.png
	$(GFX) -o $@ $<

%.pb16: %.2bpp
	$(PYTHON) _scripts/pb16.py $< $@

%.map.pb16: %.map
	$(PYTHON) _scripts/pb16.py $< $@


clean:
ifneq ("$(wildcard */*.2bpp)","")
	rm */*.2bpp
endif
ifneq ("$(wildcard */*.pb16)","")
	rm */*.pb16
endif
ifneq ("$(wildcard *.gb)","")
	rm *.gb
endif
ifneq ("$(wildcard *.sym)","")
	rm *.sym
endif
ifneq ("$(wildcard *.o)","")
	rm *.o
endif
ifneq ("$(wildcard *.map)","")
	rm *.map
endif

clean-dist: clean
ifneq ("$(wildcard *.sav)","")
	rm *.sav
endif
