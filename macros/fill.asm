fill:	MACRO
;Value, Length /to/ Destination
	ld a, \1
	ld hl, \3
	ld b, \2
	call FillMem
ENDM

longfill:	MACRO
;Value, Length /to/ Destination
	ld d, \1
	ld hl, \3
	ld bc, \2
	call FillMem2
ENDM

VRAMfill:	MACRO
;Value, Length /to/ Destination
	ld a, \1
	ld hl, \3
	ld bc, \2
	call FillVRAM
ENDM
