put:	MACRO
; value /to/ destination
	ld a, \1
	ld [\2], a
ENDM

VRAMput: MACRO
; value /to/ destination
	call WaitVRAM
	ld a, \1
	ld [\2],a
ENDM

get:	MACRO
	ld a, [\1]
ENDM
