; ===========================================================================
; Place macros includes here
; ===========================================================================

INCLUDE "./macros/enum.asm"
INCLUDE "./macros/copy.asm"
INCLUDE "./macros/fill.asm"
INCLUDE "./macros/putget.asm"


checkGB:    MACRO
; Sets zero flag if == GBC
    ld a, [wGBType]
    cp $11
ENDM

RGB: MACRO
    dw ((\3) << 10) + ((\2) << 5) + (\1)
    ENDM

waitLine:   MACRO
    ld d, \1
    call CheckLine
ENDM

wd: MACRO
    db (\1 / $100)
    db (\1 % $100)
ENDM

dbw: MACRO
    db \1
    dw \2
ENDM
