copy: MACRO
;Source, Length /to/ Destination
	ld hl, \3
	ld de, \1
	ld bc, \2
	call CopyRAM
ENDM

VRAMcopy: MACRO
;Source, Length /to/ Destination
	ld hl, \3
	ld de, \1
	ld bc, \2
	call CopyVRAM
ENDM

decompress: MACRO
;Source /to/ Destination
	ld de, \1
	ld hl, \2
	call UnPB16
ENDM

VRAMdecompress: MACRO
;Source /to/ Destination
	ld de, \1
	ld hl, \2
	call UnPB16_VRAM
ENDM

parse: MACRO
	ld de, \1
	ld hl, \2
	call ParseMap
ENDM

parse_ani: MACRO
	ld de, \1
	ld hl, \2
	call ParseMap_Animated
ENDM
