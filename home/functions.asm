SECTION "Functions", ROM0
; ---------------------------------------------------------------------------
; Fill functions
; ---------------------------------------------------------------------------
FillMem::
; Fill memory in hl
; with A for B bytes
    ld [hli], a
    dec b
    jr nz, FillMem
    ret

FillMem2::
; Fill memory in HL
; with D for BC bytes
    ld a, d
    ld [hli], a
    dec bc
    ld a, c
    or b
    jr nz, FillMem2
    ret

;ClearSprites::
;   xor a
;   ld hl, wOAMBuffer
;   ld b, 40 * 4
;   jp FillMem



; ---------------------------------------------------------------------------
; Copy functions
; ---------------------------------------------------------------------------
CopyRAM::
; Copy a bunch of data to RAM
; hl = dest
; de = src
; bc = bytecount
    ld a, [de]
    inc de
    ld [hli], a
    dec bc
    ld a, c
    or b
    jr nz, CopyRAM
    ret

CopyRAM2x::
; Copy data twice, e.g. for 1bpp art (such as fonts)
; hl = dest
; de = src
; bc = bytecount
    ld a, [de]
    inc de
    ld [hli], a         ; copy twice
    ld [hli], a
    dec bc
    ld a, c
    or b
    jr nz, CopyRAM2x
    ret



; ---------------------------------------------------------------------------
; LCD functions
; ---------------------------------------------------------------------------
DisableLCD::
; Disables LCD to allow some VRAM-related operations
; No parameters
    ld a, [rLCDC]
    rlca            ; put highest bit on carry flag
    ret nc          ; exit if screen is off already
.wait:
    ld a,[rLY]
    cp 144          ; V-blank?
    jr c,.wait      ; keep waiting if not
    ld a,[rLCDC]
    res LCD_ENABLE_BIT,a            ; reset LCD enabled flag
    ld [rLCDC],a
    ret

EnableLCD::
; Enables the GameBoy LCD
; No parameters
    ld a, [rLCDC]
    set LCD_ENABLE_BIT, a
    ld [rLCDC], a
    ret



; ---------------------------------------------------------------------------
; VRAM functions
; ---------------------------------------------------------------------------
WaitVRAM::
; Waits until it's okay to write to VRAM
; No parameters
    di
    ld a, [rSTAT]
    bit 1, a
    jr nz, WaitVRAM     ; if 1 (still used), wait
    reti

FillVRAM::
; Fill VRAM with a value for # bytes
; a  = value
; hl = dest
; bc = bytecount
; d  = backup for a
    push de
    ld d, a
.loop
    call WaitVRAM
    ld a, d
    ld [hli], a
    dec bc
    ld a, b
    or c
    jr nz, .loop
    pop de
    ret

CopyVRAM::
; Copy a bunch of data to VRAM
; hl = dest
; de = src
; bc = bytecount
    call WaitVRAM
    ld a, [de]
    inc de
    ld [hli], a
    dec bc
    ld a, c
    or b
    jr nz, CopyVRAM
    ret

GenerateMap::
; Generates a tilemap that runs sequentially and puts it, starting at hl
; a = start tile
; d = image Y size (in tiles)
; e = image X size (in tiles)
    push de
.p
    push af
    call WaitVRAM
    pop af
    ld [hli], a
    inc a
    dec e
    jr nz, .p
    pop de
    ld c, e
    push de
.q
    dec hl
    dec c
    jr nz, .q
    ld bc, $0020
    add hl, bc
    pop de
    dec d
    push de
    jr nz, .p
    pop de
    ret



; ---------------------------------------------------------------------------
; Other functions
; ---------------------------------------------------------------------------
DelayFrame::
; *The* step function
; No parameters
; affects: A
    ld a, 1                     ; let us know that we're checking for V-blank
    ld [hAskVBlank], a
.x
    halt
    ld a, [hAskVBlank]
    and a                       ; Hit V-blank?
    jr nz, .x               ; if not, keep waiting
    ret

DelayFrames::
; Delay # frames
; c = number of frames to wait
    call DelayFrame
    dec c
    jr nz,DelayFrames
    ret

GetRNG::
; RNG
; result is stored in A
    push bc
    call .getRandom
    ld c, a
    ld [wRNG_Modifier], a
    call .getRandom
    ld [wRNG_Modifier+1], a
    adc c
    pop bc
    ret
.getRandom
    ld a, [wRNG_Modifier]
    ld c, a
    ld a, [wRNG_Modifier+1]
    ld b, a
    ld a, [bc]
    ld b, a
    ld a, [rDIV]
    adc b
    ld b, a
    ld a, [rTIMA]
    adc b
    bit 5, a
    jr z, .bb
    ld c, a
    ld a, [rDIV]
    adc c
    rla
.bb
    ret

CheckLine::
; Waits until a scanline is rendering
; d = scanline # to check
    ld a, [rLY]
    cp d
    jr nz, CheckLine
    ret

ParseMap:
; A parse function using the format described below
;
; [X] [Y] [data length]
; [data]
; [X] [Y] [data length]
; [data]
; ...
; $80 (Mark data end)
;
; hl = dest.
; de = loc.
    push hl
    ld a, [de] ; read column
    ld bc, 0
    ld c, a
    add hl, bc
    inc de
    ld a, [de]  ; read row
    push de     ; de is counter now
    ld d, a
    xor a
    ld e, $20
.one
    add e
    jr nc, .onek
    inc b
.onek
    dec d
    jr nz, .one
    ld c, a
    xor a
    add hl, bc
    pop de
    inc de
    ld a, [de]  ; read data length
    ld c, a
.two
    inc de
    ld a, [de]
    push af
    call WaitVRAM
    pop af
    ld [hli], a
    dec c
    jr nz, .two
    inc de
    ld a, [de]
    cp a, $80
    pop hl
    ret z       ; return if == $FF
    jr ParseMap

ParseMap_Animated:
; parse map, row by row
;
; [X] [Y] [data length]
; [data]
; [X] [Y] [data length]
; [data]
; ...
; $80 (Mark data end)
;
; hl = dest.
; de = loc.
    push hl
    ld a, [de] ; read column
    ld bc, 0
    ld c, a
    add hl, bc
    inc de
    ld a, [de]  ; read row
    push de     ; de is counter now
    ld d, a
    xor a
    ld e, $20
.one
    add e
    jr nc, .onek
    inc b
.onek
    dec d
    jr nz, .one
    ld c, a
    xor a
    add hl, bc
    pop de
    inc de
    ld a, [de]  ; read data length
    ld c, a
.two
    inc de
    ld a, [de]
    push af
    call WaitVRAM
    pop af
    ld [hli], a
    dec c
    jr nz, .two
; DELAY 3
    call DelayFrame
    inc de
    ld a, [de]
    cp a, $80
    pop hl
    ret z       ; return if == $FF
    jr ParseMap_Animated

GetValFromTable:
; a  = value to lookup
; de = lookup table, as well as output pointer
    ld l, a
    xor a
    ld h, a
    add hl, hl
    add hl, de
    ld a, [hli]
    ld e, a
    ld a, [hl]
    ld d, a
    ret

CGBSwitchSpeed::
; switch cpu speed
    checkGB
    ret nz
    di
    put $30, rJOYP
    put 1, rKEY1
    stop
    ei
    ret

Decrement16Var::
; decrement a little endian
; 16 bit variable (hl)
; sets Z if both $00
; clobbers B
    ld a, [hl]
    sub a, 1
    ld [hl+], a
    jr nc, .checkBothZero
    ld a, [hl]
    sub a, 1
    ld [hl], a
.checkBothZero
    ld a, [hl-]
    ld b, a
    ld a, [hl]
    or b
    ret

UnPB16::
; DE -> HL
; the first byte is the number of chunks / 2
    ld a, [de]
    ld b, a
    inc de
    jp pb16_unpack_block

UnPB16_VRAM:
; DE -> HL
; clobbers BC
    push hl
    ld hl, wPB16_Buffer
    call UnPB16
    ld de, $ffff-wPB16_Buffer
    add hl, de
    push hl
    pop bc
    pop hl
    ld de, wPB16_Buffer
    jp CopyVRAM


; ---------------------------------------------------------------------------
; Switch banks
; ---------------------------------------------------------------------------
Bankswitch_Call:
; call b:hl
	ld a,[hCurrentBank]
	push af
	ld a,b
	ld [hCurrentBank],a
	ld [MBC1RomBank],a
	ld bc,.Return
	push bc
	jp hl
.Return
	pop bc
	ld a,b
	ld [hCurrentBank],a
	ld [MBC1RomBank],a
	ret

SimpleBankswitch:
	and a
	ret z	; don't do anything if 0 is passed as the argument
	ld [hCurrentBank], a
	ld [MBC1RomBank], a
	ret

PlaySong:
	ld b, a
	put BANK(DS_Init), MBC1RomBank
	ld a, b
	jp DS_Init

; ---------------------------------------------------------------------------
; Joypad functions
; ---------------------------------------------------------------------------

INCLUDE "./home/rb_joypad.asm"
