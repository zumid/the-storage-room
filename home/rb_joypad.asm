ReadJoypad::
; Poll joypad input.
; Unlike the hardware register, button
; presses are indicated by a set bit.
	ld a, 1 << 5 ; select direction keys
	ld c, 0
	ld [rJOYP], a
	rept 6
	ld a, [rJOYP]
	endr
	cpl
	and %1111
	swap a
	ld b, a
	ld a, 1 << 4 ; select button keys
	ld [rJOYP], a
	rept 10
	ld a, [rJOYP]
	endr
	cpl
	and %1111
	or b
	ld [hJoyInput], a
	ld a, 1 << 4 + 1 << 5 ; deselect keys
	ld [rJOYP], a
	ret
	
	
JoypadLowSensitivity::
	call Joypad
	ld a,[hJoy7] ; flag
	and a ; get all currently pressed buttons or only newly pressed buttons?
	ld a,[hJoyPressed] ; newly pressed buttons
	jr z,.storeButtonState
	ld a,[hJoyHeld] ; all currently pressed buttons
.storeButtonState
	ld [hJoy5],a
	ld a,[hJoyPressed] ; newly pressed buttons
	and a ; have any buttons been newly pressed since last check?
	jr z,.noNewlyPressedButtons
.newlyPressedButtons
	ld a,30 ; half a second delay
	ld [hFrameCounter],a
	ret
.noNewlyPressedButtons
	ld a,[hFrameCounter]
	and a ; is the delay over?
	jr z,.delayOver
.delayNotOver
	xor a
	ld [hJoy5],a ; report no buttons as pressed
	ret
.delayOver
; if [hJoy6] = 0 and A or B is pressed, report no buttons as pressed
	ld a,[hJoyHeld]
	and A_BUTTON | B_BUTTON
	jr z,.setShortDelay
	ld a,[hJoy6] ; flag
	and a
	jr nz,.setShortDelay
	xor a
	ld [hJoy5],a
.setShortDelay
	ld a,5 ; 1/12 of a second delay
	ld [hFrameCounter],a
	ret
	
Joypad::
; hJoyReleased: (hJoyLast ^ hJoyInput) & hJoyLast
; hJoyPressed:  (hJoyLast ^ hJoyInput) & hJoyInput

	ld a, [hJoyInput]
	;cp A_BUTTON + B_BUTTON + SELECT + START ; soft reset
	;jp z, TrySoftReset

	ld b, a
	ld a, [hJoyLast]
	ld e, a
	xor b
	ld d, a
	and e
	ld [hJoyReleased], a
	ld a, d
	and b
	ld [hJoyPressed], a
	ld a, b
	ld [hJoyLast], a

	;ld a, [wd730]
	;bit 5, a
	;jr nz, DiscardButtonPresses

	ld a, [hJoyLast]
	ld [hJoyHeld], a

	ret
	
	
	;ld a, [wJoyIgnore]
	;and a
	;ret z
	
	cpl
	ld b, a
	ld a, [hJoyHeld]
	and b
	ld [hJoyHeld], a
	ld a, [hJoyPressed]
	and b
	ld [hJoyPressed], a
	ret

;DiscardButtonPresses:
;	xor a
;	ld [hJoyHeld], a
;	ld [hJoyPressed], a
;	ld [hJoyReleased], a
;	ret

;TrySoftReset:
;	call DelayFrame

; deselect (redundant)
;	ld a, $30
;	ld [rJOYP], a

;	ld hl, hSoftReset
;	dec [hl]
;	jp z, SoftReset

;	jp Joypad
