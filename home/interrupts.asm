SECTION "vbi", ROM0
    ; Run on V-blank
    jp Vblank

SECTION "hbi", ROM0
    ; Run on H-blank
    reti

SECTION "ti",  ROM0
    ; Timer interrupt
    reti

SECTION "si", ROM0
    ; Serial interrupt
    reti

SECTION "jpi", ROM0
    ; Joypad interrupt
    reti

SECTION "Vblank", ROM0
Vblank:
	push af
	push bc
	push de
	push hl
	xor a
	ld [hAskVBlank], a

	ld a, [hSCX]
	ld [rSCX], a
	ld a, [hSCY]
	ld [rSCY], a
	ld a, [hWX]
	ld [rWX], a
	ld a, [hWY]
	ld [rWY], a

	call ReadJoypad

	put BANK(DS_Play), $2000
	call DS_Play

	checkGB
	jr nz, .notGBC

	call PushBGPals

.notGBC
; XXX rng test

	call GetRNG
	ld [wRNG_Result], a

; bankswitch back
	ld a, [hCurrentBank]
	ld [MBC1RomBank], a
	pop hl
	pop de
	pop bc
	pop af
	reti

PushBGPals:
	ld hl, wCGB_BGP
	ld b, 8*8
	ld a, $80
	ld [rBGPI], a
.copy
	ld a, [hli]
	di
;	call WaitVRAM
	ld [rBGPD], a
;	ei
	dec b
	ei
	jr nz, .copy
	ret
