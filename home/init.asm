SECTION "Init", ROM0
Init:
	ld [wGBType], a

	di					; init the whole thing
	ld sp, wStack		; set stack

	call DisableLCD

	ld a, %11100100		; set background palette
	ld [rBGP],a

	xor a
	ld [rSCX],a			; reset scroll
	ld [rSCY],a
	ld d, a

	; for title screen
	; RAM contents MUST be randomized for this to work!
	ld a, [wRAMStart]
	ld e, a

; Clear WRAM
	ld hl, W_BASE+2
	ld bc, $1FE0-2	; WRAM length
	call FillMem2

; Clear VRAM
	ld hl, vChars0
	ld bc, $2000	; VRAM length
	call FillMem2

; Clear HRAM
	ld hl, H_BASE
	ld b, $ffff - $ff80
	call FillMem

	;call ClearSprites

	ld a, e
	and a, %00000001
	ld [wTitle_Select], a

; Set initial rom bank
	ld a, 1
	ld [hCurrentBank], a

	ld a,%00000001  	; Enable V-blank interrupt
	ld [rIE], a
	ei

; set timer interval (-> RNG)
	ld a, %00000101 ; enable
	ld [rTAC], a
	jp Start
