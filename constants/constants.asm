; ===========================================================================
; Place constants includes here
; ===========================================================================

INCLUDE "./constants/hardware.asm"

W_BASE      EQU $C000
wStack      EQU $DFFF
H_BASE      EQU $FF80

DELAY_BATTERY   EQU 255

A_BUTTON EQU %00000001
B_BUTTON EQU %00000010
SELECT   EQU %00000100
START    EQU %00001000
D_RIGHT  EQU %00010000
D_LEFT   EQU %00100000
D_UP     EQU %01000000
D_DOWN   EQU %10000000

; character bits
const_value set 0
    const BIT_BUDDY	; bit 0 => 1
    const BIT_BUSTER	; bit 1 => 2
    
    const NUM_ANIMATRONICS
    ;const BIT_MYSTERY	; bit 2 => 4

BUDDY	EQU 1 << BIT_BUDDY
BUSTER	EQU 1 << BIT_BUSTER

; wControl bits
const_value set 0
    const CTL_VIEW_RIGHT_SIDE
    const CTL_MONITOR_UP
    const CTL_LEFT_DOOR_ACTIVE
    const CTL_RIGHT_DOOR_ACTIVE
    const CTL_LEFT_LIGHT_ACTIVE
    const CTL_RIGHT_LIGHT_ACTIVE
    const CTL_CAM_DISABLED
    const CTL_POSITIONS_CLEARED

; wPhase bits
const_value set 0
    const PHS_RUNOUT
    const PHS_JUMPSCARE

; controllers
const_value set 0
    const BIT_A_BUTTON
    const BIT_B_BUTTON
    const BIT_SELECT
    const BIT_START
    const BIT_D_RIGHT
    const BIT_D_LEFT
    const BIT_D_UP
    const BIT_D_DOWN

; places
const_value set 0
    const STAGE		; 0 this should be the default room to start in
    const BACKSTAGE	; 1
    const DINING_HALL	; 2
    const KITCHEN	; 3
    const STORAGE_ROOM	; 4
    const CLOSET	; 5
    const OFFICE_L	; 6
    const OFFICE_R	; 7

NUM_ROOMS EQU OFFICE_R
NUM_CAMS EQU NUM_ROOMS

; animatronic wander commands
const_value set 15
    const WANDER_LEFT
    const WANDER_RIGHT
; RANDOMIZE
; excluding office
    const RANDOMIZE
; TAKE_EITHER_OFFICE
; move to left office (or right office instead if taken)
    const TAKE_EITHER_OFFICE
; JUMP_TO_INDEX xx
; xx = *absolute* index!
    const JUMP_TO_INDEX
    const HIDE
; TRY_TAKING OFFICE
; this only works when the animatronic is in the
; left or right office
    const TRY_TAKING_OFFICE

COMMANDS_START equ WANDER_LEFT
