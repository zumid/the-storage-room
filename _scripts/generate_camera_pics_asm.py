#!/usr/bin/env python3

from argparse import ArgumentParser
import os
import re

RE_ = re.compile(r'cam-(\d+)-(\d+)\.pb16')

def make(args):
	with open(args.out, 'w') as out_file:
		# make list of files
		cam_dirs = []
		for root, dirs, files in os.walk(args.dir):
			for file_ in sorted(files):
				match = re.match(RE_, file_)
				if match:
					cam_id, anim_id = match.groups()
					file_path = os.path.join(root, file_)
					cam_dirs.append((cam_id, anim_id, file_path))
		out_file.write("; This file is automatically generated by {}\n".format(__file__))
		out_file.write("SECTION \"Camera Pictures\", ROM0\n")
		
		# make pointers
		cam_loc_ids = dict.fromkeys([x[0] for x in cam_dirs])
		for x in cam_dirs:
			if cam_loc_ids[x[0]] is None:
				cam_loc_ids[x[0]] = []
				cam_loc_ids[x[0]].append(x[1])
			else:
				cam_loc_ids[x[0]].append(x[1])
		
		out_file.write("CameraPics:\n")
		for cam_index in cam_loc_ids:
			out_file.write('\tdw Cam{}_Pics\n'.format(cam_index))
		
		for cam_index, anim_indices in cam_loc_ids.items():
			out_file.write('Cam{}_Pics:\n'.format(cam_index))
			for index in anim_indices:
				out_file.write('\tdbw BANK(PIC_Cam{0}_{1}), PIC_Cam{0}_{1}\n'.format(cam_index, index))
		
		# include the actual filesc
		ctr = 0
		ctr_= 1
		for i in cam_dirs:
			if ctr % 4 == 0:
				out_file.write("\nSECTION \"Cam Pics {}\", ROMX\n".format(ctr_))
				ctr_ += 1
			out_file.write("PIC_Cam{0}_{1}: INCBIN \"{2}\"\n".format(*i))
			ctr += 1

def main():
	ap = ArgumentParser()
	ap.add_argument('dir')
	ap.add_argument('out')
	args = ap.parse_args()
	
	make(args)

if __name__ == '__main__':
	main()
