SECTION "Title Screen", ROM0
TitleScreen:
	checkGB
	jr nz, .dmg
	copy Pal_Gameboy, 8, wCGB_BGP

.dmg
	ld a, [rLCDC]
	res 5, a
	ld [rLCDC], a	; disable window

	copy TS_Characters, \
				TS_Characters_End-TS_Characters, \
				vChars0+$D00 ; character tileset

; title screen chara
	ld a, [wTitle_Select]
	and a
	jr z, .buster
; load buddy
	copy TS_TitleBuddy, \
				TS_TitleBuddy_End-TS_TitleBuddy, \
				vChars0
	jr .fill
.buster
; load buster
	copy TS_TitleBuster, \
				TS_TitleBuster_End-TS_TitleBuster, \
				vChars0

.fill
	longfill $FF, $240, vBGMap0 ; background

; title stuff
	xor a
	ld c, 12
	ld b, 9
	ld hl, vBGMap0+$80
.row_loop
	ld [hl+], a
	inc a
	dec b
	jr nz, .row_loop
	
	rept $20 - 9
	inc hl
	endr
	
	ld b, 9
	dec c
	jr nz, .row_loop

	copy .text1, .text1end-.text1, vBGMap0+$0
	copy .text2, .text2end-.text2, vBGMap0+$20
	copy .text3, .text3end-.text3, vBGMap0+$40
	copy .text4, .text4end-.text4, vBGMap0+$140 + 9
	copy .text5, .text5end-.text5, vBGMap0+$220 + 3
	call EnableLCD

.lop
	call DelayFrame		; step event
	call JoypadLowSensitivity	; read joypad
	ld a, [hJoyPressed]
	bit BIT_START, a
	jp nz, .newgame
	jr .lop

.newgame
; Initialize game values
	put 1, wNight
	jp NightIntro

;-----------------------
.text1
	db "THE STORAGE"
.text1end
.text2
	db "ROOM"
.text2end

.text3
	db "REV. "
	db GITHASH
.text3end

.text4
	db "PRESS START"
.text4end

.text5
	db "©2018-2020 ZUMI"
.text5end
