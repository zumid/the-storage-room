SECTION "Game Loop", ROM0

; ===========================================================================
; Initialization
; ===========================================================================
InitGame:
	call DisableLCD

	ld a, [rLCDC]
	set 5, a
	ld [rLCDC], a	; enable window

; GBC check
	checkGB
	jr nz, .DMG	; if not CGB

	copy Pal_Main, 8*7, wCGB_BGP

	put 1, rVBK
	decompress PM_Office, vBGMap0
	longfill 6, $240, vBGMap1

	put 0, rVBK

	jr .DMG

.palbutton1			; A
	RGB 31,  4,  4	; Normal
	RGB 31, 16, 16	; Pressed

.palbutton2			; B
	RGB  4, 22, 31	; Normal
	RGB 17, 26, 31	; Pressed

.DMG
; determine move delay ptr
	ld a, [wNight]
	ld c, a
	ld hl, AnimatronicMoveDelays
	ld de, NUM_ANIMATRONICS
	sla e
	dec c
	jr z, .toRAM
.keepAdding
	add hl, de
	dec c
	jr nz, .keepAdding
.toRAM
	ld d, h
	ld e, l
	ld hl, wAnimatronicMoveDelayPtr
	ld a, e
	ld [hl+], a
	ld a, d
	ld [hl+], a

.initGUI
; init HUD
	put $88, hWY
	put $7, hWX

; init "battery"
	put $99, wBattery
	put DELAY_BATTERY, wDelay_Battery

; init other vars
	xor a
	ld [wControl], a
	ld [wHour], a
	ld [wCamNo], a
	ld [wPhase], a	; blackout flag
	ld [wRequestUpdateDoorPics], a

; deactivate light timers
	put $FF, wLightsTimers
	put $FF, wLightsTimers+1

; reset images
	fill $00, NUM_ROOMS + 1, wCameraPics
	fill $00, NUM_ROOMS + 1, wLastCameraPics
	fill $00, NUM_ANIMATRONICS, wAnimatronicMovementIndices
	fill $00, NUM_ANIMATRONICS, wAnimatronicPlaces

; init hours
	call ResetHourDelay

; Render office
	copy TS_Office, TS_Office_End-TS_Office, vChars0
	decompress TM_Office, vBGMap0

; init UI
	fill " ", $14, vBGMap1
	fill " ", $14, vBGMap1+$20
	fill " ", $14, vBGMap1+$220
	put "%", vBGMap1+$222
	put "X", vBGMap1+$233
;	copy TM_Night, TM_Night_End-TM_Night, vBGMap1
	copy TM_AM, TM_AM_End-TM_AM, vBGMap1+$12
	VRAMfill $FF, $9E1F-$9C40, $9C40
	call SwitchDoorPeekPics	; initialize door pic

	call EnableLCD

; init sound
	ld a,$FF
	ld [rNR51],a
	ld [rNR50],a
; office ambient sound
	xor a
	ld [SoundEnabled], a	; disable DevSound temporarily
	put $10, rNR42		; volume
	put %00000001, rNR43	; kind of static
	put %10000000, rNR44	; play static
; ===========================================================================
; Main Game Loop
; ===========================================================================
GameLoop:
	call DelayFrame			; step event
	call JoypadLowSensitivity	; read joypad

	ld a, [wPhase]
	bit PHS_RUNOUT, a
	jp nz, OffMode_Loop		; override if power ran out

	; background stuff
	call RunTimer
	call SetBatteryModifier
	call DrawStats
IF DEF(DEBUG)
	call DebugMode
ENDC

; normal operation
	call RunAnimatronics

; the jumpscare bit may be set, so make sure we jump to it immediately if it is
	ld a, [wPhase]
	bit PHS_JUMPSCARE, a
	jp nz, Jumpscared		; override on jumpscare

	ld a, [wControl]
	bit CTL_MONITOR_UP, a		; are we in monitor mode?
	jr nz, MonitorView_Loop		; if so, run the monitor loop
; ---------------------------------------------------------------------------
OfficeView_Loop:
	ld a, [hJoyInput]		; check raw input
	ld b, a

	bit BIT_D_LEFT, b
	call nz, OfficeScrollLeft	; scroll the view left

	bit BIT_D_RIGHT, b
	call nz, OfficeScrollRight	; scroll the view right

	ld a, [hJoyPressed]		; check pressed input
	bit BIT_A_BUTTON, a
	call nz, TryDoors		; turn on doors when A is pressed

	ld a, [hJoyPressed]
	bit BIT_B_BUTTON, a
	call nz, TryLights		; lights when B is pressed

	ld a, [hJoyPressed]
	bit BIT_D_UP, a
	jp nz, BringUpMonitor		; monitor when Up is pressed

	ld a, [hSCX]
	cp $32
	jr c, .notInRightSide		; if view > middle,
					; set "viewing rightside" flag
	ld a, [wControl]
	set CTL_VIEW_RIGHT_SIDE, a
	jr .setviewmode
.notInRightSide
	ld a, [wControl]
	res CTL_VIEW_RIGHT_SIDE, a

.setviewmode
	ld [wControl], a
	jr GameLoop

; ---------------------------------------------------------------------------
MonitorView_Loop:
	ld a, [hJoyPressed]		; do pressed input

	bit BIT_D_DOWN, a
	jp nz, BringDownMonitor

	bit BIT_D_LEFT, a
	call nz, SwitchCamLeft

	bit BIT_D_RIGHT, a
	call nz, SwitchCamRight

; do nothing when cam is disabled
	ld a, [wControl]
	bit CTL_CAM_DISABLED, a
	jr nz, .end

; do glitch effect when 32 < LY < 120
	waitLine 32
.distort
	ld a, [rDIV]
	and %00000011
	add 2
	ld [rWX], a
	ld a, [rDIV]
	and %00010001
	add $E4
	ld [rBGP], a
	ld a, [rLY]
	cp 120
	jr nz, .distort
	put $E4, rBGP
	put $7, rWX
; static sound
	xor a
	ld [SoundEnabled], a	; disable DevSound temporarily
	put $10, rNR42		; volume
	put %10000000, rNR43	; kind of static
	put %10000000, rNR44	; play static
.end
	jp GameLoop

; ---------------------------------------------------------------------------
OffMode_Loop:
	ld a, [hJoyInput]			; do raw input
	ld b, a
	bit BIT_D_LEFT, b
	call nz, OfficeScrollLeft
	bit BIT_D_RIGHT, b
	call nz, OfficeScrollRight

; delay and set jumpscare bit here
	ld a, [wPhase]
	bit PHS_JUMPSCARE, a
	jp nz, Jumpscared		; override on jumpscare

	jp GameLoop

; ===========================================================================
; Controls
; ===========================================================================

;; Scroll player view
OfficeScrollLeft:
	ld a, [hSCX]
	and a
	ret z	; don't scroll if scx = 0

	dec a
	dec a
	dec a
	dec a

	ld [hSCX], a
	ret

OfficeScrollRight:
	ld a, [hSCX]
	cp $60
	ret z	; don't scroll if scx = $60

	inc a
	inc a
	inc a
	inc a

	ld [hSCX], a
	ret

;; Switch cams
SwitchCamLeft:
	ld a, [wControl]
	res CTL_CAM_DISABLED,a
	ld [wControl], a

	ld a, SFX_SwitchScreens
	call PlaySong	; play screen sound

	ld a, [wCamNo]
	and a
	jr z, .max_cam	; if cam = 0, pressing left should bring the rightmost view
	dec a
	jr .switch_to_cam
.max_cam
	ld a, NUM_CAMS
.switch_to_cam
	ld [wCamNo], a
	jp LoadCamPic

SwitchCamRight:
	ld a, [wControl]
	res CTL_CAM_DISABLED, a
	ld [wControl], a

	ld a, SFX_SwitchScreens
	call PlaySong	; play screen sound

	ld a, [wCamNo]
	cp NUM_CAMS
	jr z, .cam_0	; if cam = max, pressing right should bring the 0th view
	inc a
	jr .switch_to_cam
.cam_0
	xor a
.switch_to_cam
	ld [wCamNo], a
	jp LoadCamPic

BringUpMonitor:
	ld a, SFX_PullUpMonitor
	call PlaySong	; play monitor sound

	ld a, [wControl]
	bit CTL_MONITOR_UP, a
	jp nz, GameLoop	; if monitor is already up, don't do anything
.anim
	call DelayFrame
	ld a, [hWY]

	rept 8
	dec a
	endr

	ld [hWY], a			; animate monitor being pulled up
	and a
	jr nz, .anim
	ld a, [wControl]
	set CTL_MONITOR_UP, a
	res CTL_CAM_DISABLED,a
	ld [wControl], a
	call LoadCamPic
	jp GameLoop

BringDownMonitor:
	ld a, [wControl]
	bit CTL_MONITOR_UP, a

	ld a, SFX_PullUpMonitor
	call PlaySong	; play monitor sound

	jp z, GameLoop	; if monitor already down, don't do anything
	call LoadOfficePic
	call SwitchDoorPeekPics
.anim
	call DelayFrame
	ld a, [hWY]

	rept 8
	inc a
	endr

	ld [hWY], a			; animate monitor being pulled down
	cp $88
	jr nz, .anim
	ld a, [wControl]
	res CTL_MONITOR_UP, a
	ld [wControl], a
; office ambient sound
	xor a
	ld [SoundEnabled], a	; disable DevSound temporarily
	put $10, rNR42		; volume
	put %00000001, rNR43	; kind of static
	put %10000000, rNR44	; play static
	jp GameLoop

TryDoors:
	; control doors depending on which side of the screen the player is
	; currently looking
	ld a, [wControl]
	bit CTL_VIEW_RIGHT_SIDE, a
	jr nz, .tryRight

	; left side
	bit CTL_LEFT_DOOR_ACTIVE, a
	jr nz, Open_LeftDoor	; open the door if it's active

	set CTL_LEFT_DOOR_ACTIVE, a	; close left door otherwise
	ld [wControl], a
	call TurnOff_LeftLight_NoClear
	call WaitVRAM
	put $4E, vBGMap0+($20*9)
	parse_ani TM_LeftDoor, vBGMap0-$20 ; animate left door closing
	ret

; TODO : Fix right doors and lights
.tryRight
	bit CTL_RIGHT_DOOR_ACTIVE, a
	jr nz, Open_RightDoor
	set CTL_RIGHT_DOOR_ACTIVE, a				; close right door
	ld [wControl], a
	ld a, [rBGP]			; XXX test function
	cpl				; XXX this is where
	ld [rBGP], a			; XXX turn off right light + set right door is
	call WaitVRAM
	;put $4E, vBGMap0+($20*9)
	;parse_ani TM_LeftDoor, vBGMap0-$20
	ret

Open_LeftDoor:
	ld a, [wControl]
	res CTL_LEFT_DOOR_ACTIVE, a
	ld [wControl], a
	call WaitVRAM
	put $23, vBGMap0+($20*9)
	parse_ani TM_LeftOpen, vBGMap0-$20 ; animate left door opening
	ret

Open_RightDoor:
	ld a, [wControl]
	res CTL_RIGHT_DOOR_ACTIVE, a
	ld [wControl], a
		ld a, [rBGP]			; XXX test function
		cpl				; XXX this is where
		ld [rBGP], a			; XXX unset right door is
	call WaitVRAM
	;put $23, vBGMap0+($20*9)
	;parse_ani TM_LeftOpen, vBGMap0-$20
	ret

TryLights:
	; control lights depending on which side of the screen the player is
	; currently looking
	ld a, [wControl]
	bit CTL_VIEW_RIGHT_SIDE, a
	jr nz, .tryRight

	; can't turn on the lights while the door is shut
	bit CTL_LEFT_DOOR_ACTIVE, a				; if left light
	ret nz

	; turn off the light if it is active
	bit CTL_LEFT_LIGHT_ACTIVE, a
	jr nz, TurnOff_LeftLight

	set CTL_LEFT_LIGHT_ACTIVE, a				; turn on left light
	ld [wControl], a
	call WaitVRAM
	put $4F, vBGMap0+($20*10)
	parse TM_LeftLight, vBGMap0-$20
	put 254, wLightsTimers				; run timer for left light
							; (wLightsTimers+1 = right light
	ret
.tryRight
; TODO: make right lamp control logic
	ret

TurnOff_LeftLight:
	parse TM_LeftOpen, vBGMap0-$20	; clear the door slots
TurnOff_LeftLight_NoClear:
	ld a, [wControl]
	res CTL_LEFT_LIGHT_ACTIVE, a
	ld [wControl], a
	put 255, wLightsTimers
	call WaitVRAM
	put $24, vBGMap0+($20*10)
	ret

TurnOff_RightLight:
; TODO: make logic for turning off right lamp
	ret

; ===========================================================================
; Drawing routines
; ===========================================================================
LoadCamPic:
	VRAMfill $FF, $9DD4-$9C80, $9C80
	ld a, [wCamNo]
	ld de, CameraPics
	call GetValFromTable
	; cam pics should be in de now
	ld hl, wCameraPics
	ld a, [wCamNo]
	and a
	jr z, .find_pic_index
	inc hl
	dec a
	jr nz, @-2
.find_pic_index
; hl should point to the correct address to
; determine which pic to show
	ld a, [hl]	; get index
	ld h, d
	ld l ,e		; de -> hl
	ld bc, 3
	and a
	jr z, .load_pic
	add hl, bc
	dec a
	jr nz, @-2
.load_pic
	ld a, [hl+]
	call SimpleBankswitch
	ld e, [hl]
	inc hl
	ld d, [hl]
	ld hl, vChars0
	call UnPB16_VRAM
	ld hl, vBGMap1 + $40 + 1 + $40
	xor a
	ld d, 11
	ld e, 18
	call GenerateMap
	ret

LoadOfficePic:
	VRAMfill $FF, $9E1F-$9C40, $9C40	; blank out display
	VRAMcopy TS_Office, TS_Office_End-TS_Office, vChars0
	ret

DrawStats:
; battery
	ld hl, vBGMap1 + $220
	ld a, [wBattery]
	call DrawBCD

; camname
	ld a, [wCamNo]
	ld de, CamNameIndex
	call GetValFromTable
	ld bc, 19
	ld hl, vBGMap1 + $20
	call CopyVRAM

; day
	ld a, [wNight]
	dec a
	ld de, DayNameIndex
	call GetValFromTable
	ld bc, 10
	ld hl, vBGMap1 + $00
	call CopyVRAM

; battery
	ld hl, vBGMap1 + $232
	ld a, [wBatteryModifier]
	call .draw_digit

; time
	ld hl, vBGMap1 + $0F
	ld a, [wHour]
	and a
	jr z, .midnight		; show 12am, not 0am
	ld d, a
	call WaitVRAM
	ld a, " "		; write leading space
	ld [hli], a
	ld a, d
	jr .draw_digit
.midnight
	ld a, 1
	call .draw_digit
	inc hl
	ld a, 2
; fallback to drawDigit
.draw_digit
	add "0"			; offset by "0" tile
	ld d, a
	call WaitVRAM
	ld a, d
	ld [hl], a
	ret

;; draw bcd number
DrawBCD:
	ld d, a
	and %11110000
	swap a
	add "0"
	ld c, a
	call WaitVRAM
	ld a, c
	ld [hli], a		; first digit
	ld a, d
	and %00001111
	add "0"
	ld c, a
	call WaitVRAM
	ld a, c
	ld [hli], a		; second digit
	ld a, d
	ret
; ===========================================================================
; Names / pic pointers
; ===========================================================================
CamNameIndex:
	dw .tc0
	dw .tc1
	dw .tc2
	dw .tc3
	dw .tc4
	dw .tc5
	dw .tc6
	dw .tc7
.tc0
	db "00 STAGE           "
.tc1
	db "01 BACKSTAGE       "
.tc2
	db "02 DINING HALL     "
.tc3
	db "03 KITCHEN         "
.tc4
	db "04 STORAGE ROOM    "
.tc5
	db "05 CLOSET          "
.tc6
	db "06 OFFICE LEFT     "
.tc7
	db "07 OFFICE RIGHT    "

DayNameIndex:
	dw .dn0
	dw .dn1
	dw .dn2
	dw .dn3
	dw .dn4
	dw .dn5
.dn0
	db "MONDAY    "
.dn1
	db "TUESDAY   "
.dn2
	db "WEDNESDAY "
.dn3
	db "THURSDAY  "
.dn4
	db "FRIDAY    "
.dn5
	db "SATURDAY  "

ResetHourDelay:
; set hour delay to 1min 30s (FNAFSL time)
	put $18, wDelay_Hour
	put $15,wDelay_Hour+1
	ret

; ===========================================================================
; Timer
; ===========================================================================
RunTimer:
; wait for a while before updating the battery
	ld a,[wDelay_Battery]
	dec a
	jr z, .decrease_battery
	ld [wDelay_Battery], a
	jr .runHours

.decrease_battery
	ld a, DELAY_BATTERY
	ld [wDelay_Battery], a		; reset battery timer

	ld a, [wBatteryModifier]
	ld c, a
	ld a, [wBattery]
	sub c				; *decrease*
	cp $9A
	jr nc, .doOutage	; outage if battery runs out
				; (overflows back to >$A0)
	ld b, a
	and %00001111
	cp $0A
	ld a, b
	jr c, .no_adjust_battery
	sub 6				; if right nibble >10
					; offset it back to 9
.no_adjust_battery
	ld [wBattery], a

.runHours
	ld hl, wDelay_Hour
	call Decrement16Var
	jr nz, .runLightTimer

; progress hour counter
	ld hl, wHour
	ld a, [hl]
	inc a
	cp 6	; check if 6 am
	jp z, DoSixAM
	ld [hl], a
	call ResetHourDelay
	jr .runLightTimer

.doOutage
; out of battery
	ld a, [wPhase]
	set PHS_RUNOUT, a
	ld [wPhase], a
	jr .turnOffEverything

.runLightTimer
	get wLightsTimers
	cp $FF
	jr z, .trlight
	and a
	jr nz, .tllight2
	call TurnOff_LeftLight
.tllight2
	get wLightsTimers
	dec a
	ld [wLightsTimers], a

.trlight
	get wLightsTimers+1
	cp $FF
	jr z, .lightsdone
	and a
	jr nz, .trlight2
	call TurnOff_RightLight
.trlight2
	get wLightsTimers+1
	dec a
	ld [wLightsTimers+1], a

.lightsdone
	ret

.turnOffEverything
	call Open_LeftDoor
	;call Open_RightDoor ; XXX

	checkGB
	jr nz, .turnOffEverything_dmg

; Game Boy Color
	ld b, 6
	ld hl, wCGB_BGP
.copyBlackoutPal2
	ld c, 8
	ld de, Pal_PowerOff
.copyBlackoutPal1
	ld a, [de]
	ld [hli], a
	inc de
	dec c
	jr nz, .copyBlackoutPal1
	dec b
	jr nz, .copyBlackoutPal2
	jp BringDownMonitor

.turnOffEverything_dmg
; Game Boy
	put %11111001, rBGP
	jp BringDownMonitor

; ===========================================================================
; Set battery
; ===========================================================================
SetBatteryModifier:
	xor a
	ld b, a
	ld a, [wControl]
	bit CTL_MONITOR_UP, a
	jr z, .rB_no_monitor
	inc b

.rB_no_monitor
	bit CTL_LEFT_DOOR_ACTIVE, a
	jr z, .rB_no_leftdoor
	inc b

.rB_no_leftdoor
	bit CTL_RIGHT_DOOR_ACTIVE, a
	jr z, .rB_no_rightdoor
	inc b

.rB_no_rightdoor
	bit CTL_LEFT_LIGHT_ACTIVE, a
	jr z, .rB_no_leftlight
	inc b

.rB_no_leftlight
	bit CTL_RIGHT_LIGHT_ACTIVE, a
	jr z, .rB_no_rightlight
	inc b

.rB_no_rightlight
	put b, wBatteryModifier
	ret

; ===========================================================================
; 6 AM (End of Night)
; ===========================================================================
DoSixAM:
	ld a, Song_Sixam
	call PlaySong	; play six am chime
	put $ff, hWY
	ld a, [wNight]
	inc a
	ld [wNight], a	; advance to the next night
	VRAMfill $FF, $240, vBGMap0
	put 0, hSCX
	VRAMcopy .sixAMtext, .sixAMtext_End-.sixAMtext, vBGMap0+4+($20*8)

	checkGB
	jr nz, .sixAM_notCGB

	put 1, rVBK
	VRAMfill 6, $240, vBGMap0
	put 0, rVBK

.sixAM_notCGB
	put $E4, rBGP	; pal to normal
	ld c, 255
	call DelayFrames
	ld c, 255
	call DelayFrames
	jp InitGame
.sixAMtext
	db "6 AM"
.sixAMtext_End


; ===========================================================================
; Animatronic object code
; ===========================================================================
RunAnimatronics:
; run timer
	ld c, NUM_ANIMATRONICS
	ld hl, wAnimatronicTimers
.dec_animatronic_timer
	call Decrement16Var
	jr z, .disable_cam	; if timer is out for X animatronic, move it
.nextAnimatronic
	inc hl
	inc hl
	dec c
	jr nz, .dec_animatronic_timer
; we're done; update all positions
; clear pics
	xor a
	ld hl, wCameraPics
	ld b, NUM_ROOMS+1
	call FillMem
; update data
	call UpdateCameraPics
	ld a, [wControl]
	res CTL_POSITIONS_CLEARED, a
	ld [wControl], a
; request update door sprites
	ld a, [wRequestUpdateDoorPics]
	and a
	ret z
; doing door update
	xor a
	ld [wRequestUpdateDoorPics], a
	jp SwitchDoorPeekPics

.disable_cam
; c = iterator index (2, 1, 0, -1, ...)
; Set cam disabled flag
	call DisableCamera
; save previous locations when the first animatronic moves
	ld a, [wControl]
	bit CTL_POSITIONS_CLEARED, a
	jr nz, .moveAnimatronic
	push hl
	; save previous location
		ld de, wLastCameraPics
		ld hl, wCameraPics
		ld b, NUM_ROOMS+1
		ld a, [hl+]
		ld [de], a
		inc de
		dec b
		jr nz, @-4	; ld a, [hl+]
	; don't save for the rest of the check
		ld a, [wControl]
		set CTL_POSITIONS_CLEARED, a
		ld [wControl], a
	; request door pic update once processing is done
		ld a, 1
		ld [wRequestUpdateDoorPics], a
	pop hl
.moveAnimatronic
	push bc
	; transform iterator to form
	; the index ( [2,1,0] -> [0,1,2] )
		ld a, NUM_ANIMATRONICS
		sub c
		add a
		ld c, a
		ld b, 0	; bc = 2x index ( [0,2,4] )
	; get animatronic delay
		push hl
			ld hl, wAnimatronicMoveDelayPtr	; move delays for a given day
			ld e, [hl]
			inc hl
			ld d, [hl]	; de points to the move delay location
		; add 2x index to de
			ld a, c		; add bc, de
			add e		; b is guaranteed to be 0 so we don't need to add b
			jr nc, @+3	;
			inc d		;
			ld e, a		;
		pop hl	; hl = wAnimatronicTimers
	; apply delay
		ld a, [de]
		ld [hl+], a
		inc de
		ld a, [de]
		ld [hl], a
		dec hl	; hl has to be in the original position
		push hl
			call DoAnimatronicMovement
		pop hl
	pop bc
	jr .nextAnimatronic

DoAnimatronicMovement:
; c = index * 2
; return: A  = target room index
;         C = animatronic index * 2
	ld hl, wAnimatronicMovementIndices
	srl c
	add hl, bc	; movement indices + animatronic index
	sla c ; double it again. wtf am i doing
.set_room
	push bc
	; revert C back to the original index
		push hl
			ld hl, AnimatronicPaths
			add hl, bc
			ld e, [hl]
			inc hl
			ld d, [hl]	; DE points to the start of commands for that animatronic
		pop hl
	; get index
		ld c, [hl]
	; add index to command pointer
		ld a, e		;
		add c		;
		jr nc, @+3	;
		inc d		;
		ld e, a		;
	pop bc
	; check for commands
	ld a, [de]	; get command
	cp COMMANDS_START
	jr nc, .processCommands
	inc [hl]	; increment it for the next
	jp PutAnimatronicInRoom ; A now has the target room, put them there

.processCommands
	;cp WANDER_LEFT
	;jr z, .wanderLeft
	;cp WANDER_RIGHT
	;jr z, .wanderRight
	cp RANDOMIZE
	jr z, .Randomize
	cp TAKE_EITHER_OFFICE
	jr z, .takeEitherOffice
	cp JUMP_TO_INDEX
	jr z, .jumpToIndex
	cp TRY_TAKING_OFFICE
	jr z, .tryTakingOffice
	ret	; do nothing if nothing matches

; hl = pointer to animatronic index in wram
; de = current animatronic path pointer

;.wanderLeft
;.wanderRight
.Randomize
	inc [hl]	; increment it for the next
	ld hl, wRNG_Modifier
	ld a, c
	add l
	ld l, a
	ld a, [hl]
	and 3 ; Take a value between 0 and 3
	add DINING_HALL	; resulting value is between DINING_HALL and CLOSET
	jr PutAnimatronicInRoom
.takeEitherOffice
	inc [hl]	; increment it for the next
; check if left office is used
; carry flag is set if left office is in use
	push bc
		ld hl, wAnimatronicPlaces
		ld c, NUM_ANIMATRONICS
	.look_for_left
		ld a, [hl]
		cp OFFICE_L
		jr z, .takeLeft
		inc hl
		dec c
		jr nz, .look_for_left
	pop bc
; left office not used, clear carry
	scf
	ccf	; i thought this was "clear carry flag" i'm dummy
		; clear cf = scf; ccf;
	jr .occupy
	.takeLeft
	; used, set carry
		scf
	pop bc
; take left office
; using wCameraPics since it's in the context of
; already-cleared locations
	.occupy
		jr c, .takeRightOffice
		ld a, OFFICE_L
		jr PutAnimatronicInRoom
	.takeRightOffice
	; FIXME: if both spots are taken, right office is taken
	; intended behavior: process next command when right side taken
		ld a, OFFICE_R
		jr PutAnimatronicInRoom
.jumpToIndex
	inc de
	ld a, [de]
	ld [hl], a
	jr .set_room	; hl is still the same, so we're fine running it again
	ret
.tryTakingOffice
	inc [hl]	; increment it for the next
; check left office
	push bc
		srl c
		call SetAppropriateBit
		ld c, a
		ld a, [wLastCameraPics + OFFICE_L]
		and c
	pop bc
	jr z, .checkRightOffice
	ld a, [wControl]
	bit CTL_LEFT_DOOR_ACTIVE, a
	jr nz, .checkRightOffice	; skip if left door closed
	; animatronic in left office && is the correct animatronic
	jr .setJumpPic
	.checkRightOffice
		push bc
			srl c
			call SetAppropriateBit
			ld c, a
			ld a, [wLastCameraPics + OFFICE_R]
			and c
		pop bc
		jr z, .skipTakingOffice
		ld a, [wControl]
		bit CTL_RIGHT_DOOR_ACTIVE, a
		jr nz, .skipTakingOffice	; skip if right door closed
		; animatronic in right office && is the correct animatronic
	.setJumpPic
		ld a, c
		srl a
		ld [wScarePic], a	; set jumpscare pic
		ld a, [wPhase]		;
		set PHS_JUMPSCARE, a	;
		ld [wPhase], a		; set jumpscare flag
	; we don't jumpscare in here since that will probably fuck up the stack
	; instead we set a flag to be checked by the main loop
		ret
	.skipTakingOffice
	; instantly process next command
		inc [hl]
		inc de
		ld a, [de]
		jp .processCommands

PutAnimatronicInRoom:
; a = room index
; c = animatronic index * 2
; clobbers b
	ld b, a
	ld hl, wAnimatronicPlaces
	srl c		; c / 2
	ld a, l
	add c
	jr nc, @+3
	inc h		; hl + c
	ld l, a
	ld a, b
	ld [hl], a
	ret

SwitchDoorPeekPics:
; check left door
	ld a, [wCameraPics + OFFICE_L]
	bit BIT_BUDDY, a
	jr nz, .buddy_on_left
	bit BIT_BUSTER, a
	jr nz, .buster_on_left
; nothing_on_left
	VRAMcopy TS_OfficeDoorL_0, TS_OfficeDoorL_0_End-TS_OfficeDoorL_0, vChars0+$500
	jr .check_right
.buddy_on_left
	VRAMcopy TS_OfficeDoorL_1, TS_OfficeDoorL_1_End-TS_OfficeDoorL_1, vChars0+$500
	jr .check_right
.buster_on_left
	VRAMcopy TS_OfficeDoorL_2, TS_OfficeDoorL_2_End-TS_OfficeDoorL_2, vChars0+$500
	; check_right fallthru
.check_right
	ret

SetAppropriateBit:
; A = 1 << C
; Ret: C = 0
	inc c
	ld a, 1
.loop
	dec c
	ret z
	sla a
	jr .loop

UpdateCameraPics:
; not gonna bother reversing the index this time
	ld c, NUM_ANIMATRONICS
.loop
	ld hl, wCameraPics
	ld de, wAnimatronicPlaces
	dec c
	ld a, e
	add c
	jr nc, @+3
	inc d
	inc c
	ld e, a
	ld a, [de]
	and a
	jr z, .roomZero
	inc hl
	dec a
	jr nz, @-2
.roomZero
	ld b, c
	dec c
	call SetAppropriateBit
	ld c, b
	ld b, [hl]
	add b
	ld [hl], a
	dec c
	ret z
	jr .loop
; ===========================================================================
; Camera control
; ===========================================================================
DisableCamera:
	ld a, [wControl]
	bit CTL_MONITOR_UP, a
	ret z			; don't do anything if monitor is not up
				; this flag gets reset upon monitor up anyway
	bit CTL_CAM_DISABLED, a
	ret nz			; same goes if the cam is already disabled
	push hl
	push bc
		VRAMfill $FF, $9E1F-$9C40, $9C40	; blank out display
		VRAMcopy .discam_text, .discam_text_End-.discam_text, vBGMap1+4+($20*8)
	pop bc
	pop hl
	ld a, [wControl]
	set CTL_CAM_DISABLED, a
	ld [wControl], a
	ret
.discam_text
	db "NO SIGNAL"
.discam_text_End

; ===========================================================================
; Do jumpscare
; ===========================================================================
Jumpscared:
	; TODO: make it load graphics
	longfill $FF,  $9a40-$9800, $9800
	longfill $FF,  $1000,       $8000
	xor a
	ld [hSCX], a	; reset X
	ld a, [rLCDC]
	res 5, a
	ld [rLCDC], a	; disable window
	; load image here
	ld a, [wScarePic]
	ld c, 120
	call DelayFrames
	jp Start
IF DEF(DEBUG)
; ===========================================================================
; Tiny debug mode
; ===========================================================================
DebugMode:
; display animatronic locations
	ld b, NUM_ROOMS+1
	ld hl, wCameraPics
	ld de, vBGMap1+7
.loop
	ld a,[hl+]
	ld c, $EA
	add c
	ld c, a
	call WaitVRAM
	ld a, c
	ld [de], a
	inc de
	dec b
	jr nz, .loop

; hax
; anim. 1
	ld a, [wAnimatronicTimers+0+1]
	ld c, $EA
	add c
	ld c, a
	call WaitVRAM
	ld a, c
	ld [$980e], a
	ld [$9de6], a
	ld a, [wAnimatronicTimers+0+0]
	and %11110000
	swap a
	ld c, $EA
	add c
	ld c, a
	call WaitVRAM
	ld a, c
	ld [$980f], a
	ld [$9de7], a
	ld a, [wAnimatronicTimers+0+0]
	and %00001111
	ld c, $EA
	add c
	ld c, a
	call WaitVRAM
	ld a, c
	ld [$9810], a
	ld [$9de8], a
; anim. 2
	ld a, [wAnimatronicTimers+2+1]
	ld c, $EA
	add c
	ld c, a
	call WaitVRAM
	ld a, c
	ld [$982e], a
	ld [$9e06], a
	ld a, [wAnimatronicTimers+2+0]
	and %11110000
	swap a
	ld c, $EA
	add c
	ld c, a
	call WaitVRAM
	ld a, c
	ld [$982f], a
	ld [$9e07], a
	ld a, [wAnimatronicTimers+2+0]
	and %00001111
	ld c, $EA
	add c
	ld c, a
	call WaitVRAM
	ld a, c
	ld [$9830], a
	ld [$9e08], a
	ret
ENDC
