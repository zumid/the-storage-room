SECTION "Night Intro", ROM0
NightIntro:
	VRAMfill $FF, $240, vBGMap0

	get wNight
	dec a
	ld c, a
	xor a
	ld b, a
	ld hl, .texts
	add hl, bc
	add hl, bc
	ld a, [hl+]
	ld e, a
	ld a, [hl+]
	ld d, a

	ld hl, vBGMap0+4+($20*8)

	ld a, [de]
	inc de
	and a
	jr z, .finishposition
.position
	inc hl
	dec a
	jr z, .finishposition
	jr .position

.finishposition
	ld a, [de]
	inc de
	cp "@"
	jr z, .finish
	ld b, a
	call WaitVRAM
	ld a, b
	ld [hl+], a
	jr .finishposition

.finish
	ld c, 120
	call DelayFrames
; apply start delays
	ld hl, AnimatronicStartDelays
	xor a
	ld b, a
	ld a, [wNight]
	dec a
	sla a
	ld c, a
	add hl, bc
	ld d, h
	ld e, l
	ld hl, wAnimatronicTimers
	ld bc, NUM_ANIMATRONICS * 2
	call CopyRAM
; apply initial locations
	xor a	; ld a, STAGE
	ld [wAnimatronicPlaces + BIT_BUDDY], a
	ld [wAnimatronicPlaces + BIT_BUSTER], a
	call UpdateCameraPics
	jp InitGame

.texts
	dw .t1
	dw .t2
	dw .t3
	dw .t4
	dw .t5
	dw .t6
	dw .t7
.t1
	db 1,"MONDAY@"
.t2
	db 1,"TUESDAY@"
.t3
	db 0,"WEDNESDAY@"
.t4
	db 0,"THURSDAY@"
.t5
	db 1,"FRIDAY@"
.t6
	db 0,"SATURDAY@"
.t7
	db 1,"SUNDAY@"
