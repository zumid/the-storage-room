SECTION "Tilesets",ROM0

TS_Office:
INCBIN "./gfx/office.2bpp"
INCBIN "./gfx/officedoorsandlights.2bpp"
TS_Office_End:

TS_Characters:
INCBIN "./gfx/alphabet.2bpp"
TS_Characters_End:

TS_TitleBuster:
INCBIN "./gfx/titlescreen_buster.2bpp"
TS_TitleBuster_End:

TS_TitleBuddy:
INCBIN "./gfx/titlescreen_buddy.2bpp"
TS_TitleBuddy_End:

TS_OfficeDoorL_0:	; no one
INCBIN "./gfx/office-door-left-0.2bpp"
TS_OfficeDoorL_0_End:
TS_OfficeDoorL_1:	; buddy
INCBIN "./gfx/office-door-left-1.2bpp"
TS_OfficeDoorL_1_End:
TS_OfficeDoorL_2:	; buster
INCBIN "./gfx/office-door-left-2.2bpp"
TS_OfficeDoorL_2_End:
