SECTION "Palettes",ROM0

Pal_Gameboy:
    RGB 31,31,31
    RGB 17,24,14
    RGB 6,13,10
    RGB 0,0,0
Pal_Main:
;0
    RGB 31, 31, 31
    RGB 28, 15, 26
    RGB 13,  9, 16
    RGB  6,  1,  3
;1
    RGB 31, 26, 26
    RGB 31, 25,  7
    RGB  9, 17,  7
    RGB  0,  0,  0
;2
    RGB 31, 31, 31
    RGB 31, 20, 20
    RGB 27, 12, 12
    RGB  0,  0,  0
;3
    RGB 31, 31, 31
    RGB 31, 27,  7
    RGB 16, 11, 11
    RGB  0,  0,  0
;4
    RGB 31, 31, 31
    RGB 31, 16, 16  ; Pressed
    RGB 31,  4,  4
    RGB  0,  0,  0
;5
    RGB 31, 31, 31
    RGB 17, 26, 31  ; Pressed
    RGB  4, 22, 31
    RGB  0,  0,  0
;6
    RGB 31, 31, 31
    RGB 16, 16, 16
    RGB 8, 8, 8
    RGB  0,  0,  0

Pal_PowerOff:
;0
    RGB 9, 9, 16
    RGB 4, 4, 9
    RGB 3, 3, 7
    RGB 1, 2, 5

