SECTION "Animatronic Data",ROM0

AnimatronicStartDelays:
; structured 0-1-2-3-4
; XXX make it so each day has the right values
    dw $0056, $0056 ; monday
    dw $2000, $2000 ; tuesday
    dw $0C00, $0C00 ; wednesday
    dw $0500, $0500 ; thursday
    dw $0300, $0300 ; friday
    dw $0000, $0000 ; sunday

AnimatronicMoveDelays:
; structured 0-1-2-3-4
; XXX make it so each day has the right values
    dw $0056, $0056 ; monday
    dw $0200, $0200 ; tuesday
    dw $0100, $0100 ; wednesday
    dw $0100, $0100 ; thursday
    dw $0100, $0100 ; friday
    dw $0100, $0100 ; sunday

AnimatronicPaths:
    dw .buster
    dw .buddy

.buster
; initial path
    ; db STAGE			; This is implied
    db DINING_HALL		; 0
    db KITCHEN			; 1
; path loop
    db RANDOMIZE		; 2
    db RANDOMIZE		; 3
    db RANDOMIZE		; 4
    db TAKE_EITHER_OFFICE	; 5
    db JUMP_TO_INDEX, 3	; 6

.buddy
; initial path
    ; db STAGE			; This is implied
    db DINING_HALL		; 0
    db KITCHEN			; 1
; path loop
    db RANDOMIZE		; 2
    db RANDOMIZE		; 3
    db RANDOMIZE		; 4
    db TAKE_EITHER_OFFICE	; 5
    db JUMP_TO_INDEX, 3	; 6
