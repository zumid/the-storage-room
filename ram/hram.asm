; ---------------------------------------------------------------------------
; HRAM
; ---------------------------------------------------------------------------
SECTION "HRAM", HRAM
hRAMStart:		ds 1		; beginning of HRAM
hAskVBlank:		ds 1		; VBlank flag

;hInput:			ds 1		; joypad
;hInputLast:		ds 1		; joypad
;hInputPress:	ds 1		; joypad
;hInputRelease:	ds 1		; joypad
;hInputHold:		ds 1		; joypad

hJoyInput:	ds 1
hJoyLast:	ds 1
hJoyReleased:	ds 1
hJoyPressed:	ds 1
hJoyHeld:	ds 1
hJoy5:	ds 1
hJoy6:	ds 1
hJoy7:	ds 1
hFrameCounter:	ds 1
hCurrentBank:	ds 1

hSCX:			ds 1		; input registers ...
hSCY:			ds 1
hWX:			ds 1
hWY:			ds 1

hPB16Byte0:	ds 1
