; ---------------------------------------------------------------------------
; WRAM
; ---------------------------------------------------------------------------
SECTION "WRAM", WRAM0
wRAMStart::		ds 1		; beginning of WRAM
;wOAMBuffer::	ds 4 * 40	; buffer for OAM data. Copied to OAM by DMA

wGBType::	ds 1

wControl::
; bit 0: if viewing right side of office, is 1
; bit 1: monitor pulled up?
; bit 2: left door?
; bit 3: right door?
; bit 4: left light?
; bit 5: right light?
; bit 6: camera disabled by anim. moving?
; bit 7: anim. positions already cleared?
	ds 1

wNight::	; Night #
	ds 1
wHour::		; Hour #
	ds 1
wCamNo::	; Current camera view
	ds 1

wBattery::	; Encoded in BCD
	ds 1
wBatteryModifier::
	ds 1

wDelay_Battery::
	ds 1

wDelay_Hour::
	ds 2

wPhase::	; power phase
; 0 = normal
; bit 0 = run out of battery
; bit 1 = awaiting jumpscare
; bit 2 = jumpscared
	ds 1

wAnimatronicMoveDelayPtr:
	ds 2	; AnimatronicMoveDelays

wCameraPics:
; each byte for the room
; 8 bits defining present animatronics
; also defines which picture to use
; 0000 0000
	ds NUM_ROOMS + 1

wLastCameraPics:
; each byte for the room
; 8 bits defining present animatronics
; also defines which picture to use
; 0000 0000
	ds NUM_ROOMS + 1

wAnimatronicTimers:
	ds NUM_ANIMATRONICS * 2

wAnimatronicMovementIndices:
	ds NUM_ANIMATRONICS

wAnimatronicPlaces:
; each byte represents one animatronic
; which room is it in?
	ds NUM_ANIMATRONICS

wRequestUpdateDoorPics:
; set to 1 if requesting to update the door pic
; once it updates, it clears again
	ds 1

wLightsTimers:
; 0000 0000   0000 0000
; L E F T     R I G H T
	ds 2

wCGB_BGP::
	ds 8*8

wRNG_Modifier::
	ds 2
wRNG_Result::
	ds 1

wTitle_Select::
	ds 1

wScarePic::
; which animatronic's pic to use
	ds 1

wPB16_Buffer::
	ds $c60
